
:: Setup Kickass Environment

@set KICKASS_HOME=%~dp0
@set KICKASS_PATH=%KICKASS_HOME%..\lib

::  Create Alias for using Kickass Assembler from CLI

@doskey kick=java -jar %KICKASS_PATH%\kickass.jar $*

